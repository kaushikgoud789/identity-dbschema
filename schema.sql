CREATE TABLE address(
    id serial PRIMARY KEY,
    streetaddress VARCHAR NOT NULL,
    city VARCHAR NOT NULL,
    state VARCHAR NOT NULL,
    zipcode integer NOT NULL,
    country VARCHAR NOT NULL,
    landmark VARCHAR,
    dateupdated TIMESTAMPTZ );

CREATE TABLE organisation(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    description VARCHAR,
    streetaddress VARCHAR NOT NULL,
    city VARCHAR NOT NULL,
    state VARCHAR NOT NULL,
    zipcode integer NOT NULL,
    country VARCHAR NOT NULL,
    landmark VARCHAR,
    dateupdated TIMESTAMPTZ);

    
CREATE TABLE department(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    description VARCHAR,
    dateupdated TIMESTAMPTZ);
    
CREATE TABLE person_role(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    description VARCHAR,
    dateupdated TIMESTAMPTZ);  
    
    
CREATE TABLE lab(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    description VARCHAR,
    dateupdated TIMESTAMPTZ);    

CREATE TABLE person(
    id serial PRIMARY KEY,
    firstname VARCHAR NOT NULL,
    middlename VARCHAR,
    lastname VARCHAR NOT NULL,
    cellphone VARCHAR,
    homephone VARCHAR,
    officephone VARCHAR,
    emailaddress VARCHAR,  
    organisation_id int,
    department_id int,
    designation VARCHAR NOT NULL,
    person_role int,
    supervisor int, 
    dateupdated TIMESTAMPTZ,
    FOREIGN KEY(organisation_id) REFERENCES organisation(id),
    FOREIGN KEY(department_id) REFERENCES department(id),
    FOREIGN KEY(person_role) REFERENCES person_role(id),
    FOREIGN KEY(supervisor) REFERENCES person(id));
 
CREATE TABLE person_address(
    person_id int,
    address_id int,
    dateupdated TIMESTAMPTZ,
    PRIMARY KEY (person_id,address_id),     
    FOREIGN KEY (address_id) REFERENCES address(id),     
    FOREIGN KEY (person_id) REFERENCES person(id));  
 
CREATE TABLE credentials(
    id serial PRIMARY KEY,
    person_id int,
    username VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    isactive time NOT NULL,
    lastreset TIMESTAMPTZ,
    dateupdated TIMESTAMPTZ,
    FOREIGN KEY (person_id) REFERENCES person(id));

    
CREATE TABLE department_lab(
    department_id int,
    lab_id int,
    PRIMARY KEY (department_id,lab_id),     
    FOREIGN KEY (department_id) REFERENCES department(id),     
    FOREIGN KEY (lab_id) REFERENCES lab(id));